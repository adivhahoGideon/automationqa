package com.way2Automation.webTable.stepDef;


import com.way2Automation.webTable.helper.AddUserHelper;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class webTableStepDef {


    WebDriver driver;
    private AddUserHelper helper;

    public webTableStepDef(){
        helper=new AddUserHelper();

    }


    @Before(value="@addNewUser")
    public void setup(){
        System.setProperty("webdriver.chrome.driver","C:\\Users\\Command Quality\\Desktop\\Gaserve 2019\\QATask02\\src\\test\\resources\\drivers\\chromedriver.exe");
        this.driver=new ChromeDriver();
        this.driver.manage().window().maximize();
        this.driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

    }

    @After(value="@addNewUser")
    public void cleanUp(){
        driver.manage().deleteAllCookies();
        driver.close();
        driver.quit();
    }

    @Given("^I navigate to \"([^\"]*)\"$")
    public void i_navigate_to(String link) throws Throwable {
        driver.get(link);
    }

    @Given("^I am on User List Table Page$")
    public void i_am_on_User_List_Table_Page() throws Throwable {

        Assert.assertTrue(driver.findElement(By.xpath("/html/body/table/thead/tr[2]/td/button")).isDisplayed());
    }

    @When("^I add new users \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void iAddNewUsers(String firstName, String lastname, String userName, String password, String customer, String Role, String email, String cellphone) throws Throwable {
        helper.addUser(driver,firstName,lastname,userName,password,customer,Role,email,cellphone);
    }

    @And("^username are unique\"([^\"]*)\"$")
    public void usernameAreUnique(String userName) throws Throwable {
        Assert.assertTrue(helper.uniqueUsername(userName));
    }

    @Then("^The Usernames should appear on the list\"([^\"]*)\"$")
    public void theUsernamesShouldAppearOnTheList(String userName) throws Throwable {
        Assert.assertTrue(helper.uniqueUsername(userName));

    }
}
