package com.way2Automation.webTable.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)

@CucumberOptions(
        features = {"src/test/resources/FeatureFile/webTable.feature"},
        glue = {"com.way2Automation.webTable.stepDef"},
        monochrome = true,
        tags = {"@addNewUser"},
        plugin = {"pretty", "html:target/cucumber","json:target/cucumber.json", "com.cucumber.listener.ExtentCucumberFormatter:target/report.html"}
)


public class webTableRunner {
}
