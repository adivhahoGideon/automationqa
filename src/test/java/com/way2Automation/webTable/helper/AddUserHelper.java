package com.way2Automation.webTable.helper;

import com.way2Automation.webtables.AddUserPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AddUserHelper {

    private AddUserPage addUserPage;

    public AddUserHelper(){

    }

    public void addUser(WebDriver driver,String firstName, String lastname, String userName, String password, String customer, String Role, String email, String cellphone){
        driver.findElement(By.xpath("/html/body/table/thead/tr[2]/td/button")).click();
        addUserPage=new AddUserPage(driver.switchTo().defaultContent());
        addUserPage.enterName(firstName);
        addUserPage.lastName(lastname);
        addUserPage.userName(userName);
        addUserPage.password(password);
        addUserPage.company(customer);
        addUserPage.roleID(Role);
        addUserPage.email(email);
        addUserPage.mobilePhone(cellphone);
        driver.findElement(By.xpath("//*[contains(text(),'Save')]")).click();
    }

    public Boolean uniqueUsername(String username){
        return addUserPage.uniqueUsername(username);
    }
}
