@addNewUser
Feature: Successfully add new user

Scenario Outline: Add new User to users table
  Given I navigate to "<link>"
  And I am on User List Table Page
  When I add new users "<firstName>" "<lastname>" "<userName>" "<password>" "<customer>" "<Role>" "<email>" "<cellphone>"
  And username are unique"<userName>"
  Then The Usernames should appear on the list"<userName>"


  Examples:
  |link                                                             |firstName|lastname|userName|password|customer   |Role     |email             |cellphone|
  | http://www.way2automation.com/angularjs-protractor/webtables/   |cliff    |ford    |clive   |1234    |CompanyAAA |Customer |clive.ford@cq.com |11111111 |
  | http://www.way2automation.com/angularjs-protractor/webtables/   |why      |when    |who     |2222    |CompanyBBB |Admin    |ford@mail.com     |12121212 |