package com.way2Automation.webtables;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class AddUserPage {


  private WebDriver driver;


    public AddUserPage(WebDriver driver){
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }

    public void enterName(String name){ driver.findElement(By.name("FirstName")).sendKeys(name);}

    public void lastName(String lastName){
        driver.findElement(By.name("LastName")).sendKeys(lastName);
    }

    public void userName(String userName){
        driver.findElement(By.name("UserName")).sendKeys(userName);
    }

    public void password(String password){
        driver.findElement(By.name("UserName")).sendKeys(password);
    }

    public void company(String company){
        if(company.equalsIgnoreCase("companyAAA")){
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/form/table/tbody/tr[5]/td[2]/label[1]/input")).click();
        }else if(company.equalsIgnoreCase("companyBBB")){
            driver.findElement(By.xpath("/html/body/div[3]/div[2]/form/table/tbody/tr[5]/td[2]/label[2]")).click();
        }
    }


    public void roleID(String role){
        Select dropDown= new Select(driver.findElement(By.name("RoleId")));
        dropDown.selectByVisibleText(role);
    }

    public void email(String email){
        driver.findElement(By.name("Email")).sendKeys(email);
    }

    public void mobilePhone(String mobilephone){
        driver.findElement(By.name("Mobilephone")).sendKeys(mobilephone);
    }

    public Boolean uniqueUsername(String username){
        List<WebElement> tableRows=driver.findElements(By.xpath("/html/body/table/tbody/tr"));
        int totalNumOfRows=tableRows.size();
        int sameUsernameCount=0;
        int count=1;

        while(count<totalNumOfRows){
            if(driver.findElement(By.xpath("/html/body/table/tbody/tr["+count+"]/td[3]")).getText().equals(username)){
                sameUsernameCount++;
            }
            count++;
        }
        if(sameUsernameCount>=2){
            return false;
        }else{
        return true;}
    }

}
